/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: ["class"],
  content: [
    './pages/**/*.{ts,tsx}',
    './components/**/*.{ts,tsx}',
    './app/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}',
  ],
  theme: {
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
      },
    },
    extend: {
      fontFamily: {
        Chillax: ['Chillax', 'sans-serif'],
        sans: ['Chillax', 'sans-serif'],
      },
      colors: {
        'primary': '#16181E',
        'secondary': '#21242D',
        'custom-blue': '#00B6F0',
        'custom-yellow': "#FFEB00",
        'custom-danger': '#D93805',
        'custom-text': "#F9F9F9"
      },
      gradientColorStops: {
        'dark-red': '#D93805',
        'light-red': '#FF3D00',
        'dark-yellow': '#FFE600',
        'light-yellow': '#E1D000',
        'dark-blue': '#00B6F0',
        'light-blue': '#52D5FF'
      },
    },
  },
  plugins: [require("tailwindcss-animate")],
}