import { HeaderAccountCard } from "./subcomponents/HeaderAccountCard"
import { HeaderBrand } from "./subcomponents/HeaderBrand"
import { HeaderLoginRegisterAction } from "./subcomponents/HeaderLoginRegisterAction"
import { HeaderRoot } from "./subcomponents/HeaderRoot"

export const Header = {
    Root: HeaderRoot,
    Brand: HeaderBrand,
    Account: HeaderAccountCard,
    LoginRegister: HeaderLoginRegisterAction
}