import { ReactNode } from "react";

interface RootProps {
  children: ReactNode;
}

export function HeaderRoot({ children }: RootProps) {
  return (
    <div className="w-full h-16 bg-blue-500  flex justify-between">
      {children}
    </div>
  );
}
